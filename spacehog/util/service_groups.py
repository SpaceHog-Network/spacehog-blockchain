from typing import KeysView, Generator

SERVICES_FOR_GROUP = {
    "all": "spacehog_harvester spacehog_timelord_launcher spacehog_timelord spacehog_farmer spacehog_full_node spacehog_wallet".split(),
    "node": "spacehog_full_node".split(),
    "harvester": "spacehog_harvester".split(),
    "farmer": "spacehog_harvester spacehog_farmer spacehog_full_node spacehog_wallet".split(),
    "farmer-no-wallet": "spacehog_harvester spacehog_farmer spacehog_full_node".split(),
    "farmer-only": "spacehog_farmer".split(),
    "timelord": "spacehog_timelord_launcher spacehog_timelord spacehog_full_node".split(),
    "timelord-only": "spacehog_timelord".split(),
    "timelord-launcher-only": "spacehog_timelord_launcher".split(),
    "wallet": "spacehog_wallet spacehog_full_node".split(),
    "wallet-only": "spacehog_wallet".split(),
    "introducer": "spacehog_introducer".split(),
    "simulator": "spacehog_full_node_simulator".split(),
}


def all_groups() -> KeysView[str]:
    return SERVICES_FOR_GROUP.keys()


def services_for_groups(groups) -> Generator[str, None, None]:
    for group in groups:
        for service in SERVICES_FOR_GROUP[group]:
            yield service


def validate_service(service: str) -> bool:
    return any(service in _ for _ in SERVICES_FOR_GROUP.values())
