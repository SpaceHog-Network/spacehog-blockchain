from typing import Dict

# The rest of the codebase uses piglets everywhere.
# Only use these units for user facing interfaces.
units: Dict[str, int] = {
    "spacehog": 10 ** 12,  # 1 spacehog (HOG) is 1,000,000,000,000 piglet (1 trillion)
    "piglet:": 1,
    "colouredcoin": 10 ** 3,  # 1 coloured coin is 1000 colouredcoin piglets
}
