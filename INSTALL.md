# Installation

Install instructions have been moved to the [INSTALL](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki/INSTALL) section of the repository [Wiki](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki).

After installing, follow the remaining instructions in the
[Quick Start Guide](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki/Quick-Start-Guide)
to run the software.
