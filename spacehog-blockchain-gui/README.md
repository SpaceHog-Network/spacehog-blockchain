# spacehog-blockchain
![Alt text](https://www.spacehog.xyz/img/spacehog_logo.svg)

![GitHub contributors](https://img.shields.io/github/contributors/SpaceHog-Network/spacehog-blockchain?logo=GitHub)

Please check out the [wiki](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki)
and [FAQ](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki/FAQ) for
information on this project.

## Installing

This is the GUI for spacehog-blockchain. It is built into distribution packages in the spacehog-blockchain repository.

Install instructions are available in the
[INSTALL](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki/INSTALL)
section of the
[spacehog-blockchain repository wiki](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki).

## Running

Once installed, a
[Quick Start Guide](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki/Quick-Start-Guide)
is available from the repository
[wiki](https://bitbucket.com/SpaceHog-Network/spacehog-blockchain/wiki).
