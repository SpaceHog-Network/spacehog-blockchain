# Localization

Thanks for helping to translate the GUI for SpaceHog Blockchain.

Please head over to our [Crowdin project](https://crowdin.com/project/spacehog-blockchain/) and add/edit translations there.
