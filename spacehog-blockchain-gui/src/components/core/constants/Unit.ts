enum Unit {
  SPACEHOG = 'SPACEHOG',
  PIGLET = 'PIGLET',
  COLOURED_COIN = 'COLOUREDCOIN',
}

export default Unit;
