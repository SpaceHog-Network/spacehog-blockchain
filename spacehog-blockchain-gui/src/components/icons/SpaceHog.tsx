import React from 'react';
import { SvgIcon, SvgIconProps } from '@material-ui/core';
import { ReactComponent as SpaceHogIcon } from './images/spacehog.svg';

export default function Keys(props: SvgIconProps) {
  return <SvgIcon component={SpaceHogIcon} viewBox="0 0 150 58" {...props} />;
}
