import React, { useMemo } from 'react';
import { Trans } from '@lingui/macro';
import { useSelector } from 'react-redux';
import type { RootState } from '../../../modules/rootReducer';
import FarmCard from './FarmCard';
import { piglet_to_spacehog } from '../../../util/spacehog';
import useCurrencyCode from '../../../hooks/useCurrencyCode';

export default function FarmCardTotalSpaceHogFarmed() {
  const currencyCode = useCurrencyCode();

  const loading = useSelector(
    (state: RootState) => !state.wallet_state.farmed_amount,
  );

  const farmedAmount = useSelector(
    (state: RootState) => state.wallet_state.farmed_amount?.farmed_amount,
  );

  const totalSpaceHogFarmed = useMemo(() => {
    if (farmedAmount !== undefined) {
      const val = BigInt(farmedAmount.toString());
      return piglet_to_spacehog(val);
    }
  }, [farmedAmount]);

  return (
    <FarmCard
      title={<Trans>{currencyCode} Total SpaceHog Farmed</Trans>}
      value={totalSpaceHogFarmed}
      loading={loading}
    />
  );
}
