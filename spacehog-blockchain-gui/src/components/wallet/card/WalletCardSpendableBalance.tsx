import React from 'react';
import { Trans } from '@lingui/macro';
import FarmCard from '../../farm/card/FarmCard';
import useWallet from '../../../hooks/useWallet';
import useCurrencyCode from '../../../hooks/useCurrencyCode';
import { piglet_to_spacehog_string } from '../../../util/spacehog';

type Props = {
  wallet_id: number;
};

export default function WalletCardSpendableBalance(props: Props) {
  const { wallet_id } = props;

  const { wallet, loading } = useWallet(wallet_id);
  const currencyCode = useCurrencyCode();

  const value = wallet?.wallet_balance?.spendable_balance;

  return (
    <FarmCard
      loading={loading}
      valueColor="secondary"
      title={<Trans>Spendable Balance</Trans>}
      tooltip={
        <Trans>
          This is the amount of SpaceHog that you can currently use to make
          transactions. It does not include pending farming rewards, pending
          incoming transactions, and SpaceHog that you have just spent but is not
          yet in the blockchain.
        </Trans>
      }
      value={
        <>
          {piglet_to_spacehog_string(value)} {currencyCode}
        </>
      }
    />
  );
}
