const Big = require('big.js');
const units = require('./units');

// TODO: use bigint instead of float
const convert = (amount, from, to) => {
  if (Number.isNaN(Number.parseFloat(amount)) || !Number.isFinite(amount)) {
    return 0;
  }

  const amountInFromUnit = Big(amount).times(units.getUnit(from));

  return Number.parseFloat(amountInFromUnit.div(units.getUnit(to)));
};

class SpaceHog {
  constructor(value, unit) {
    this._value = value;
    this._unit = unit;
  }

  to(newUnit) {
    this._value = convert(this._value, this._unit, newUnit);
    this._unit = newUnit;

    return this;
  }

  value() {
    return this._value;
  }

  format() {
    const displayUnit = units.getDisplay(this._unit);

    const { format, fractionDigits, trailing } = displayUnit;

    let options = { maximumFractionDigits: fractionDigits };

    if (trailing) {
      options = { minimumFractionDigits: fractionDigits };
    }

    let value;

    if (fractionDigits !== undefined) {
      const fractionPower = Big(10).pow(fractionDigits);
      value = Number.parseFloat(
        Big(Math.floor(Big(this._value).times(fractionPower))).div(
          fractionPower,
        ),
      );
    } else {
      value = this._value;
    }

    let formatted = format.replace(
      '{amount}',
      Number.parseFloat(value).toLocaleString(undefined, options),
    );

    if (displayUnit.pluralize && this._value !== 1) {
      formatted += 's';
    }

    return formatted;
  }

  toString() {
    const displayUnit = units.getDisplay(this._unit);
    const { fractionDigits } = displayUnit;
    const options = { maximumFractionDigits: fractionDigits };
    return Number.parseFloat(this._value).toLocaleString(undefined, options);
  }
}

export const spacehog_formatter = (value, unit) => new SpaceHog(value, unit);

spacehog_formatter.convert = convert;
spacehog_formatter.setDisplay = units.setDisplay;
spacehog_formatter.setUnit = units.setUnit;
spacehog_formatter.getUnit = units.getUnit;
spacehog_formatter.setFiat = (currency, rate, display = null) => {
  units.setUnit(currency, 1 / rate, display);
};

export const piglet_to_spacehog = (piglet) => {
  return spacehog_formatter(Number.parseInt(piglet), 'piglet').to('spacehog').value();
};

export const spacehog_to_piglet = (spacehog) => {
  return spacehog_formatter(Number.parseFloat(Number(spacehog)), 'spacehog')
    .to('piglet')
    .value();
};

export const piglet_to_spacehog_string = (piglet) => {
  return spacehog_formatter(Number(piglet), 'piglet').to('spacehog').toString();
};

export const piglet_to_colouredcoin = (piglet) => {
  return spacehog_formatter(Number.parseInt(piglet), 'piglet')
    .to('colouredcoin')
    .value();
};

export const colouredcoin_to_piglet = (colouredcoin) => {
  return spacehog_formatter(Number.parseFloat(Number(colouredcoin)), 'colouredcoin')
    .to('piglet')
    .value();
};

export const piglet_to_colouredcoin_string = (piglet) => {
  return spacehog_formatter(Number(piglet), 'piglet').to('colouredcoin').toString();
};
