const spacehog = require('../../util/spacehog');

describe('spacehog', () => {
  it('converts number piglet to spacehog', () => {
    const result = spacehog.piglet_to_spacehog(1000000);

    expect(result).toBe(0.000001);
  });
  it('converts string piglet to spacehog', () => {
    const result = spacehog.piglet_to_spacehog('1000000');

    expect(result).toBe(0.000001);
  });
  it('converts number piglet to spacehog string', () => {
    const result = spacehog.piglet_to_spacehog_string(1000000);

    expect(result).toBe('0.000001');
  });
  it('converts string piglet to spacehog string', () => {
    const result = spacehog.piglet_to_spacehog_string('1000000');

    expect(result).toBe('0.000001');
  });
  it('converts number spacehog to piglet', () => {
    const result = spacehog.spacehog_to_piglet(0.000001);

    expect(result).toBe(1000000);
  });
  it('converts string spacehog to piglet', () => {
    const result = spacehog.spacehog_to_piglet('0.000001');

    expect(result).toBe(1000000);
  });
  it('converts number piglet to colouredcoin', () => {
    const result = spacehog.piglet_to_colouredcoin(1000000);

    expect(result).toBe(1000);
  });
  it('converts string piglet to colouredcoin', () => {
    const result = spacehog.piglet_to_colouredcoin('1000000');

    expect(result).toBe(1000);
  });
  it('converts number piglet to colouredcoin string', () => {
    const result = spacehog.piglet_to_colouredcoin_string(1000000);

    expect(result).toBe('1,000');
  });
  it('converts string piglet to colouredcoin string', () => {
    const result = spacehog.piglet_to_colouredcoin_string('1000000');

    expect(result).toBe('1,000');
  });
  it('converts number colouredcoin to piglet', () => {
    const result = spacehog.colouredcoin_to_piglet(1000);

    expect(result).toBe(1000000);
  });
  it('converts string colouredcoin to piglet', () => {
    const result = spacehog.colouredcoin_to_piglet('1000');

    expect(result).toBe(1000000);
  });
});
